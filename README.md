Olá Voxus, 

Esse sistema foi desenvolvido em PHP e JSON, ele realilza um CRUD de tasks conforme o cenário proposto para o desafio.

O sistema não precisa de instalação,  ele está hospedado na WEB portanto será necessário o uso da internet para acessá-lo. Ele está hospedado na 
Hostinger contendo um servidor de MySQL-MariaDB e um servidor Apache para o PHP.

Na tela inicial eu coloquei a autenticação com o google, depois dessa autenticação você é redirecionado para a tela inicial com o nome de CRUD.php.
Esta tela contém a inserção de tasks e também a funcionalidade de listar todas as tasks cadastradas. No lado esquerdo coloquei um menu simples
para as funcionalidades de excluir e alterar tasks. 

A maioria das funcionalidades utilizam o numero da task como identificador, então para excluir você informa o número da task e para alterar a mesma
forma.

Eu demorei 7 horas para desenvolver o back-end, o front-end ficou simples porque eu me preocupei em estruturar o código (dá muito mais trabalho haha).
Aprendi muito com as autenticações do google e sei que dá pra fazer um sistema robusto com essas autenticações. 

Gostei do desafio, agradeço desde já!!
Qualquer dúvida estou à disposição.
meu e-mail: matheusbezerra2309@gmail.com