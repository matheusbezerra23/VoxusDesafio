<?php
 
 session_start();
 
 if (isset( $_SESSION["name"])){
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once '../model/db_connect.php';
 
// connecting to db
$connection = new DB_CONNECT();
$connection->connect();

// get all products from products table
$result = mysqli_query($connection->myconn, "SELECT * FROM TB_TASKS");
 
// check for empty result
if (mysqli_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["tasks"] = array();
 
    while ($row = mysqli_fetch_array($result)) {
        // temp user array
        $tasks = array();
        $tasks["ID_TASK"] = $row["ID_TASK"];
        $tasks["NOME_TASK"] = $row["NOME_TASK"];
        $tasks["DESC_TASK"] = $row["DESC_TASK"];
        $tasks["PRIORIDADE_TASK"] = $row["PRIORIDADE_TASK"];
        $tasks["USUARIO_TASK"] = $row["USUARIO_TASK"];
        $tasks["NR_TASK"] = $row["NR_TASK"];
        $tasks["DATA_TASK"] = $row["DATA_TASK"];
 
        // push single product into final response array
        array_push($response["tasks"], $tasks);
    }
    // success
    $response["success"] = 1;
 
    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "Nenhuma taks encontrada";
 
    // echo no users JSON
    echo json_encode($response);

    $result->close();
}

}else{
    echo "else";
    header('Location: ../Index.php'); 
 }
