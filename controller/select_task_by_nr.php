<?php

session_start();

if (isset( $_SESSION["name"])){
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once '../model/db_connect.php';
 
// connecting to db
$connection = new DB_CONNECT();
$connection->connect();

if (isset($_GET['nr_task'])){

$nr_task = $_GET['nr_task']; 
$result = mysqli_query($connection->myconn, "SELECT * FROM TB_TASKS WHERE NR_TASK=" . $nr_task);
 
// check for empty result
    if (mysqli_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["tasks"] = array();
 
        while ($row = mysqli_fetch_array($result)) {
            // temp user array
            $tasks = array();
            $tasks["ID_TASK"] = $row["ID_TASK"];
            $tasks["NOME_TASK"] = $row["NOME_TASK"];
            $tasks["DESC_TASK"] = $row["DESC_TASK"];
            $tasks["PRIORIDADE_TASK"] = $row["PRIORIDADE_TASK"];
            $tasks["USUARIO_TASK"] = $row["USUARIO_TASK"];
            $tasks["NR_TASK"] = $row["NR_TASK"];
            $tasks["DATA_TASK"] = $row["DATA_TASK"];
 
            // push single product into final response array
            array_push($response["tasks"], $tasks);
        }
        // success
        $response["success"] = 1;
    
        // echoing JSON response
        
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "Nenhuma taks encontrada";
    
        // echo no users JSON
    }
echo "
    <head>
    <meta charset='UTF-8'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' integrity='sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb' crossorigin='anonymous'>
    </head> 
    <link href='css/style.css' type='text/css' rel='stylesheet'>
    <body>
    <style>
    body{
        font-family: arial, helvetica, sans-serif;
        font-size: 12px;
    }
    .form1{
        text-align: center;
        margin-top: 1em;
    }
    input, textarea {
        font: 1em sans-serif;
        width: 300px;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        border: 1px solid #999;
    }
    div.center{
        background-color: #eceff1;
        position: absolute;
        left: 240px;
        top: 80px;
        width: 870px;
        height: 500px;
    }
    div.left{
        background-color: #8B0000;
        position: absolute;
        left: 25px;
        top: 80px;
        width: 200px;
        height: 500px;
    
    }
    p {
        text-align: right;
        padding-right: 340px;
    }
    li a {
        text-decoration: none;
        color: #EEE;
    }
    li a:hover{
    background-color: #B22222;
    padding: 20px; 	
    }
    li {
        width: 200px;
        height: 40px;
        color: #EEE;
        text-align: center;
    }
    ul{
     list-style: none;
    padding-left: 0px;
    list-style: none;
    margin-top: 40px;
    }
    </style>

    <script type='text/javascript' language='javascript'>
        function valida_form(){
            if(document.getElementById('nome_task').value.length < 1 || 
            document.getElementById('desc_task').value.length < 1 ||
            document.getElementById('prioridade_task').value.length < 1 ||
            document.getElementById('nr_task').value.length < 1 ){
            alert('Por favor, preencha o campo nome');
            document.getElementById('nome_task').focus();
        return false;
        } else {
            alert('Task alterada com sucesso');
            document.getElementById('form1').submit();   
        }
        }
    </script>
    <div class='center'>";


if($response["success"] == 1){
    echo "<form name='form1' class='form1' action='update_task.php' method='post'>
            <input type='text' name='id_task' value='". $tasks["ID_TASK"]. "'>
            <p>Nome task: <input type='text' id='nome_task' value='" .$tasks["NOME_TASK"]. "' name='nome_task' /></p>
            <p>Descrição da task: <input type='text' id='desc_task' value='" .$tasks["DESC_TASK"]. "' name='desc_task' /></p>
            <p>Prioridade da task: <input type='text' id='prioridade_task' value='" . $tasks["PRIORIDADE_TASK"]. "' name='prioridade_task' /></p>
            <p>Numero da task<input type='text' id='nr_task' value='" .$tasks["NR_TASK"]. "' name='nr_task'/></p>
            <button class='btn btn-primary' onclick='valida_form()'>inserir</button>
    </form>";
}else {
        echo '<td>Nenhuma task encontrada</td>';
}
echo '</table>
        </div>';

echo "<div class='left'>
        <ul>
        <li><a href='../view/CRUD.php'>Inserir/Listar Tasks </a></li>
        <li><a href='../view/delete_CRUD.php'>Deletar Task</a></li>
        <li><a href='../view/update_CRUD.php'>Alterar Task</a></li>
        </ul>
    </div>";
echo '</body>';
}

}else{
    echo "else";
    header('Location: ../Index.php'); 
 }
