<?php

session_start();

if (isset( $_SESSION["name"])){

 header('content-type: application/json; charset=utf-8');
 header("access-control-allow-origin: *");
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

    //campos de envio via formulario
if (isset($_POST['nome_task']) && isset($_POST['desc_task']) && isset($_POST['prioridade_task']) &&
    isset($_POST['nr_task']) ) {
        
    $nome_task = $_POST['nome_task']; 
    $nr_task = $_POST['nr_task'];
    $desc_task = $_POST['desc_task'];
    $prioridade_task = $_POST['prioridade_task'];
    $usuario = "Matheus";
    // include db connect class
    require_once '../model/db_connect.php';
 
    // connecting to db
    $cnn = new DB_CONNECT();
    $link = $cnn->connect();
    
    // insert a new row
    if ($cnn->querydb("set @id_task = '';") &&
        $cnn->querydb("CALL PRC_CAD_TASK('" . $nome_task .  "'," . "'" . $desc_task . "',"
        . "'" . $prioridade_task . "'," .
        "'" . $usuario . "'," .
        "'" . $nr_task . "'," .
        "@id_task);")){
    
            if($res = $cnn->querydb("select  @id_task as id_task")){
                while ($row = mysqli_fetch_array($res)){
                    $response['success'] = 1;
                    $response['id_task'] = $row['id_task'];
                } 
                echo json_encode($response);
            }
    // check if row inserted or not
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["id_task"] = null;
        $response["mensagem"] = "Ocorreu um" . mysqli_error($link). " erro ao inserir";
        // echoing JSON response
        echo json_encode($response);
    } 
    
}else{
    // required field is missing
    $response["success"] = 0;
    $response["id_task"] = null;
    $response["mensagem"] = "campos obrigatorios nao preenchidos";
    // echoing JSON response
    echo json_encode($response);
}

header('Location: ../view/CRUD.php'); 

}else{
    echo "else";
    header('Location: ../Index.php'); 
 }
