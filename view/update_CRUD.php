<?php

session_start();

if (isset( $_SESSION["name"])){
echo "
    <head>
    <meta charset='UTF-8'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' integrity='sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb' crossorigin='anonymous'>
    </head> 
    <link href='css/style.css' type='text/css' rel='stylesheet'>
    <body>
    <style>
    body{
        font-family: arial, helvetica, sans-serif;
        font-size: 12px;
    }
    .form1{
        text-align: center;
        margin-top: 1em;
    }
    input, textarea {
        font: 1em sans-serif;
        width: 300px;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        border: 1px solid #999;
    }
    div.center{
        background-color: #eceff1;
        position: absolute;
        left: 240px;
        top: 80px;
        width: 870px;
        height: 500px;
    }
    div.left{
        background-color: #8B0000;
        position: absolute;
        left: 25px;
        top: 80px;
        width: 200px;
        height: 500px;
    
    }
    p {
        text-align: right;
        padding-right: 340px;
    }
    li a {
        text-decoration: none;
        color: #EEE;
    }
    li a:hover{
    background-color: #B22222;
    padding: 20px; 	
    }
    li {
        width: 200px;
        height: 40px;
        color: #EEE;
        text-align: center;
    }
    ul{
     list-style: none;
    padding-left: 0px;
    list-style: none;
    margin-top: 40px;
    }
    </style>

    <script type='text/javascript' language='javascript'>
        function valida_form(){
            if(document.getElementById('nr_task').value.length < 1 ){
                alert('Por favor, preencha o campo número da task');
                document.getElementById('nr_task').focus();
                return false;
            } else {
                document.getElementById('form1').submit();
                alert('task alterada com sucesso');
            }
        }
    </script>
    <div class='center'>
    <form name='form1' class='form1' action='../controller/select_task_by_nr.php' method='GET'>
        <p>Número da task: <input type='text' id='nr_task' name='nr_task' /></p>
        <button class='btn btn-primary' onclick='valida_form()'>pesquisar</button>
    </form>";
    
echo "</div><div class='left'>
    <ul>
    <li><a href='CRUD.php'>Inserir/Listar Tasks </a></li>
    <li><a href='delete_CRUD.php'>Deletar Task</a></li>
    <li><a href='update_CRUD.php'>Alterar Task</a></li>
    </ul>
</div>";
echo '</body>';

}else{
    echo "else";
    header('Location: ../Index.php'); 
 }
