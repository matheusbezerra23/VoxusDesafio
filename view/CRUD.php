<?php

session_start();

if (isset( $_SESSION["name"])){
   
echo "
    <head>
    <meta charset='UTF-8'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' integrity='sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb' crossorigin='anonymous'>
    </head> 
    <link href='css/style.css' type='text/css' rel='stylesheet'>
    <body>
    <style>
    
    body{
        font-family: arial, helvetica, sans-serif;
        font-size: 12px;
    }
    .form1{
        text-align: center;
        margin-top: 30px;
    }
    input, textarea {
        font: 1em sans-serif;
        width: 300px;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        border: 1px solid #999;
    }
    div.center{
        background-color: #eceff1;
        position: absolute;
        left: 240px;
        top: 80px;
        width: 870px;
        height: 500px;
    }
    div.left{
        background-color: #8B0000;
        position: absolute;
        left: 25px;
        top: 80px;
        width: 200px;
        height: 500px;
    
    }
    p {
        text-align: right;
        padding-right: 340px;
    }

    a{
        text-decoration: none;
    }
    li a {
        text-decoration: none;
        color: #EEE;
    }
    li a:hover{
    background-color: #B22222;
    padding: 20px; 	
    }
    li {
        width: 200px;
        height: 40px;
        color: #EEE;
        text-align: center;
    }
    ul{
     list-style: none;
    padding-left: 0px;
    list-style: none;
    margin-top: 40px;
    }
    </style>

    <script type='text/javascript' language='javascript'>
        function valida_form(){
            if(document.getElementById('nome_task').value.length < 3 || 
            document.getElementById('desc_task').value.length < 3 ||
            document.getElementById('prioridade_task').value.length < 3 ||
            document.getElementById('nr_task').value.length < 3 ){
            alert('Por favor, preencha o campo nome');
            document.getElementById('nome_task').focus();
        return false;
        } else {
            document.getElementById('form1').submit();
        }
    }
    </script>";
     $var = $_SESSION["name"];
    echo "<p> Olá <b>" . $var . " </b> Seja Bem vindo</p> 
    <div class='center'>
    <form name='form1' class='form1' action='../controller/create_task.php' method='post'>
        <p>Nome task: <input type='text' id='nome_task' name='nome_task' /></p>
        <p>Descrição da task: <input type='text'name='desc_task' /></p>
        <p>Prioridade da task: <input type='text' name='prioridade_task' /></p>
        <p>Numero da task<input type='text' name='nr_task'/></p>
        <button class='btn btn-primary' onclick='valida_form()'>inserir</button>
    </form>";



$content = file_get_contents("http://marvlogrotas.esy.es/VoxusDesafio/controller/select_task.php");

$data = json_decode($content);
//print_r($myarray["tasks"]); // Access Array data
echo '</br></br></br></br></br><p>Lista de Tasks - Aqui aparecerão as Tasks cadastradas</p>';
echo '<table>';
echo '<tr><td>Nome</td><td>Descrição</td><td>Prioridade</td>
        <td>Usuário</td><td>Número</td><td>Data</td></tr>';

 if($data->success == 1){
    foreach($data->tasks as $task) {
        echo '<tr>';
        echo '<td>'. $task->NOME_TASK . '</td>';
        echo '<td>'. $task->DESC_TASK . '</td>';
        echo '<td>'. $task->PRIORIDADE_TASK . '</td>';
        echo '<td>'. $task->USUARIO_TASK . '</td>';
        echo '<td>'. $task->NR_TASK . '</td>';
        echo '<td>'. $task->DATA_TASK . '</td>';
        echo '</tr>';
    }
 } else{
    echo '<tr>';
    echo '<td>Nenhuma task encontrada</td>';
    echo '</tr>';
 }
echo '</table>
        </div>';

echo "<div class='left'>
        <ul>
        <li><a href='CRUD.php'>Inserir/Listar Tasks </a></li>
        <li><a href='delete_CRUD.php'>Deletar Task</a></li>
        <li><a href='update_CRUD.php'>Alterar Task</a></li>
        </ul>
    </div>";
echo '</body>';

}else{
    echo "else";
    header('Location: ../Index.php'); 
 }